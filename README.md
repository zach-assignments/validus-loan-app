# validus-loan-app

Web application that allows users to 1. login, 2. apply for a loan, and 3. see a list of all submitted loans. Built on Vue.js.